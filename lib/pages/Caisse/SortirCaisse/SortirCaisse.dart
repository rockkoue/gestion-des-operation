import 'package:caisse/pages/Caisse/SortirCaisse/widgets/HomeSortirCaisse.dart';
import 'package:caisse/pages/Home/widgets/FooterNavigation.dart';
import 'package:flutter/material.dart';

class SortirCaisse extends StatelessWidget {
  const SortirCaisse({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          title: const Text('Sortir de Caisse'),
          backgroundColor: Color.fromARGB(255, 236, 148, 15)),
      body: const HomeSortirCaisse(),
      bottomNavigationBar: const FooterNavigation(),
    );
  }
}
