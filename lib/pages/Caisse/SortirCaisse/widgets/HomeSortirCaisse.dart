// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';

class HomeSortirCaisse extends StatelessWidget {
  const HomeSortirCaisse({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(children: [
        SizedBox(height: 20),
        Image.asset(
          'assets/images/deposit.gif',
          height: 100,
          width: 100,
        ),
        SizedBox(height: 20),
        Container(
            width: 300,
            child: Column(
              children: [
                Text(
                  'Montant',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                TextField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.currency_exchange),
                    hintText: 'Entrer un montant ',
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'Description',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.details_outlined),
                    hintText: 'Merci de donnée des details',
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {},
                  child: const Text('ENREGISTER UNE SORTIE'),
                ),
              ],
            )),
        SizedBox(height: 20),
      ]),
    );
  }
}
