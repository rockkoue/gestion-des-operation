// ignore_for_file: prefer_const_constructors

import 'package:caisse/pages/Caisse/EntrerCaisse/widgets/HomeEntrerCaisse.dart';
import 'package:caisse/pages/Home/widgets/FooterNavigation.dart';
import 'package:flutter/material.dart';

class EnterCaisse extends StatelessWidget {
  const EnterCaisse({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Entrée de Caisse'),
          backgroundColor: Color.fromARGB(255, 236, 148, 15)),
      body: const HomeEnterCaisse(),
      bottomNavigationBar: const FooterNavigation(),
    );
  }
}
