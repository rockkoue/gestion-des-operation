// ignore_for_file: prefer_const_constructors, deprecated_member_use

import 'package:caisse/pages/Caisse/EntrerCaisse/EntrerCaisse.dart';
import 'package:caisse/pages/Caisse/SortirCaisse/SortirCaisse.dart';
import 'package:flutter/material.dart';

class HomeCaisse extends StatelessWidget {
  const HomeCaisse({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          SizedBox(height: 20),
          Image.asset(
            'assets/images/img_purse_1.png',
            width: 100, // Adjust size as needed
            height: 100,
          ),
          SizedBox(height: 120),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            EnterCaisse()), // Replace OtherPage() with the page you want to navigate to
                  );
                },
                style: ElevatedButton.styleFrom(
                  primary:
                      Color.fromARGB(255, 243, 170, 33), // Background color
                  onPrimary: Colors.white, // Text color
                  padding: EdgeInsets.symmetric(
                      horizontal: 20, vertical: 10), // Button padding
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(10)), // Button border radius
                ),
                child: const Text('ENTRER EN CAISSE'),
              ),
              SizedBox(width: 20), // Add some space between the buttons
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            SortirCaisse()), // Replace OtherPage() with the page you want to navigate to
                  );
                },
                style: ElevatedButton.styleFrom(
                  primary:
                      Color.fromARGB(255, 243, 170, 33), // Background color
                  onPrimary: Colors.white, // Text color
                  padding: EdgeInsets.symmetric(
                      horizontal: 20, vertical: 10), // Button padding
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(10)), // Button border radius
                ),
                child: const Text('SORTIR DE CAISSE'),
              ),
            ],
          ),
          SizedBox(height: 20),
          Image.asset(
            'assets/images/img_analysis_1.png',
            height: 150,
            width: 150,
          ),
          SizedBox(
            height: 10,
          ), // Add some space between the image and text
          Text(
            'Bilan', // Add text for the first image
            textAlign: TextAlign.center, // Align the text in the center
          ),
        ],
      ),
    );
  }
}
