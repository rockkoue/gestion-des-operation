import 'package:caisse/pages/Caisse/widgets/HomeCaisse.dart';
import 'package:caisse/pages/Home/widgets/FooterNavigation.dart';
import 'package:flutter/material.dart';

class Caisse extends StatelessWidget {
  const Caisse({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Caisse'),
          backgroundColor: Color.fromARGB(255, 236, 148, 15)),
      body: const HomeCaisse(),
      bottomNavigationBar: const FooterNavigation(),
    );
  }
}
