// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:caisse/pages/Caisse/caisse.dart';
import 'package:caisse/pages/Home/widgets/FooterNavigation.dart';
import 'package:caisse/pages/Moto/Moto.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: const IconButton(
              icon: Icon(Icons.menu),
              tooltip: 'Navigation menu',
              onPressed: null,
            ),
            title: const Text('Manager Caisse'),
            backgroundColor: Color.fromARGB(255, 236, 148, 15)),
        body: Center(
          child: Column(
            mainAxisAlignment:
                MainAxisAlignment.center, // Center the column vertically
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment:
                      MainAxisAlignment.center, // Center the row horizontally
                  children: [
                    Expanded(
                        child: GestureDetector(
                      onTap: () {
                        // Action to perform when the first image is clicked
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Caisse()),
                        );
                      },
                      child: Column(
                        children: [
                          SizedBox(
                            height: 250,
                          ),
                          Image.asset(
                            'assets/images/img_purse_1.png',
                            width: 100, // Adjust size as needed
                            height: 100,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            'Gestion Caisse', // Add text for the first image
                            textAlign: TextAlign
                                .center, // Align the text in the center
                          ),
                        ],
                      ),
                    )),
                    SizedBox(width: 20),
                    Expanded(
                        child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Moto()),
                        );
                      },
                      child: Column(
                        children: [
                          SizedBox(
                            height: 250,
                          ),
                          Image.asset(
                            'assets/images/delivery-bike.png', // Replace with your second image path
                            width: 100, // Adjust size as needed
                            height: 100,
                          ),
                          SizedBox(
                            height: 20,
                          ), // Add some space between the image and text
                          Text(
                            'Gestion Moto tricycle', // Add text for the second image
                            textAlign: TextAlign
                                .center, // Align the text in the center
                          ),
                        ],
                      ),
                    )),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: const FooterNavigation());
  }
}
