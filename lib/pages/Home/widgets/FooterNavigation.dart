import 'package:flutter/material.dart';

class FooterNavigation extends StatefulWidget {
  const FooterNavigation({super.key});

  @override
  State<FooterNavigation> createState() => _FooterNavigationState();
}

class _FooterNavigationState extends State<FooterNavigation> {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.login_rounded),
          label: 'Logout',
        ),
      ],
      selectedItemColor: Colors.amber[800],
    );
  }
}
