import 'package:caisse/pages/Home/widgets/FooterNavigation.dart';
import 'package:caisse/pages/Moto/widgets/homemoto.dart';

import 'package:flutter/material.dart';

class Moto extends StatelessWidget {
  const Moto({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Moto'),
          backgroundColor: Color.fromARGB(255, 236, 148, 15)),
      body: Homemoto(),
      bottomNavigationBar: const FooterNavigation(),
    );
  }
}
