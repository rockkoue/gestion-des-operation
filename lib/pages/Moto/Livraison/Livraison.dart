import 'package:caisse/pages/Home/widgets/FooterNavigation.dart';
import 'package:caisse/pages/Moto/Livraison/widgets/homelivraison.dart';

import 'package:flutter/material.dart';

class Livraison extends StatelessWidget {
  const Livraison({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Livraison'),
          backgroundColor: Color.fromARGB(255, 236, 148, 15)),
      body: Homelivraison(),
      bottomNavigationBar: const FooterNavigation(),
    );
  }
}
