// ignore_for_file: prefer_const_constructors, sort_child_properties_last, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';

class Homedepense extends StatefulWidget {
  const Homedepense({super.key});

  @override
  State<Homedepense> createState() => _HomedepenseState();
}

class _HomedepenseState extends State<Homedepense> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: SizedBox(
              height: 100,
              child: Center(
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Image.asset(
                        'assets/images/img_policeman_1.png',
                        height: 80,
                        width: 80,
                        fit: BoxFit.cover,
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Image.asset(
                        'assets/images/img_car_repair_1.png',
                        height: 80,
                        width: 80,
                        fit: BoxFit.cover,
                      ),
                    ),
                    // Add some space between the image and text

                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Image.asset(
                        'assets/images/img_search_1.png',
                        height: 80,
                        width: 80,
                        fit: BoxFit.cover,
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Image.asset(
                        'assets/images/img_gas_pump_1.png',
                        height: 80,
                        width: 80,
                        fit: BoxFit.cover,
                      ),
                    ),
                    // Add some space between the image and text
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 50),
          Container(
              width: 300,
              child: Column(
                children: [
                  Text(
                    'Montant',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  TextField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.currency_exchange),
                      hintText: 'Entrer un montant ',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    'Selection type depense',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  DropdownButtonFormField(
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.details_outlined),
                      border: OutlineInputBorder(),
                    ),
                    items: [
                      DropdownMenuItem(
                          child: Text('Assurance '), value: 'Option 1'),
                      DropdownMenuItem(
                          child: Text('Reparation'), value: 'Option 3'),
                      DropdownMenuItem(
                          child: Text('Police'), value: 'Option 2'),
                      DropdownMenuItem(
                          child: Text('Reparation'), value: 'Option 3'),
                    ],
                    onChanged: (value) {
                      // Handle selected option
                    },
                  ),
                  SizedBox(height: 20),
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text('ENREGISTER UNE DEPENSE'),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
