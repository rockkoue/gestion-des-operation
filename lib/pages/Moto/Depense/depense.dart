import 'package:caisse/pages/Home/widgets/FooterNavigation.dart';
import 'package:caisse/pages/Moto/Depense/Widgets/homedepense.dart';
import 'package:flutter/material.dart';

class Depense extends StatelessWidget {
  const Depense({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('Depense'),
          backgroundColor: Color.fromARGB(255, 236, 148, 15)),
      body: Homedepense(),
      bottomNavigationBar: const FooterNavigation(),
    );
  }
}
